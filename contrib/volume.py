#!/usr/bin/env python3
# Copyright (C) 2024 Riften Labs AS,
# This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.,
# A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

from urllib.request import urlopen
import json

url = "http://indexer.cauldron.quest/cauldron/contract/volume?end=9999999999"

with urlopen(url) as response:
    data = json.load(response)
    total_satoshis = sum(item["total_sats"] for item in data)
    print(f"Total BCH: {total_satoshis / 100000000}")


