#!/usr/bin/env python3
# Copyright (C) 2024 Riften Labs AS,
# This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.,
# A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

import os
import sys
import re

"""
Dissallow println/print statements. These are usually leftovers from debugging.
In case they are not; info! debug! etc macros are preferred.
"""

OUR_PATH = os.path.dirname(os.path.realpath(__file__))

def check_file_for_prints(file_path):
    """Check if the file contains println! or print! outside of test modules."""
    with open(file_path, 'r', encoding='utf-8') as file:
        for line_num, line in enumerate(file, 1):
            if re.search(r"\bmod tests \{", line):
                break  # Stop checking further if inside test module

            if re.search(r"println!\(", line) or re.search(r"print!\(", line):
                print(f"{file_path}:{line_num}: print statement found")
                return False

    return True

def check_files(directories):
    """
    Traverse the specified directories and check each .rs file.
    """
    found_forbidden_prints = False

    for directory in directories:
        for root, dirs, files in os.walk(os.path.join(OUR_PATH, "..", directory)):
            for file in files:
                if file.endswith('.rs'):
                    file_path = os.path.join(root, file)
                    if not check_file_for_prints(file_path):
                        found_forbidden_prints = True

    if found_forbidden_prints:
        print("Forbidden print statements found.")
        sys.exit(1)
    else:
        print("OK")
        sys.exit(0)

check_files(['src', 'contrib', 'linters'])

