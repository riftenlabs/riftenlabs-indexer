#!/usr/bin/env python3
# Copyright (C) 2024 Riften Labs AS,
# This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.,
# A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

import os
import subprocess
import sys

def run_python_file(filepath):
    result = subprocess.run(filepath)
    return result.returncode == 0

def run_all_linters_in_folder(folder_path):
    failures = 0
    for filename in os.listdir(folder_path):
        # Ensure we're not attempting to run this script itself
        if filename.endswith('.py') and filename != os.path.basename(__file__):
            filepath = os.path.join(folder_path, filename)
            print(f"Running linter: {filename}")
            if not run_python_file(filepath):
                failures += 1

    if failures != 0:
        print(f"{failures} linter(s) failed")
        sys.exit(1)

if __name__ == "__main__":
    folder_path = os.path.dirname(os.path.realpath(__file__))
    run_all_linters_in_folder(folder_path)

