#!/usr/bin/env python3
# Copyright (C) 2024 Riften Labs AS,
# This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.,
# A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

import os
import sys

required_lines = [
    "Copyright (C) 2024 Riften Labs AS",
    "This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.",
    "A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html"
]

OUR_PATH = os.path.dirname(os.path.realpath(__file__))

def check_file_for_lines(file_path, lines):
    """Check if the file contains all the specified lines."""
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
        return all(line in content for line in lines)

def check_files(directories):
    """Traverse the src directory and check each .rs file."""
    missing = 0

    for directory in directories:
        for root, dirs, files in os.walk(os.path.join(OUR_PATH, "..", directory)):
            for file in files:
                if file.endswith('.rs') or file.endswith('.py'):
                    file_path = os.path.join(root, file)
                    if not check_file_for_lines(file_path, required_lines):
                        print(f"{file_path}: Missing")
                        missing += 1

    if missing:
        print(f"{missing} file(s) are missing copyright headers")
        sys.exit(1)
    else:
        print(f"OK")
        sys.exit(0)

check_files(['src', 'contrib', 'linters'])

