// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use bitcoin_hashes::hash_newtype;
use bitcoin_hashes::sha256d;

hash_newtype!(
    PoolID,
    sha256d::Hash,
    32,
    doc = "An identifier for a Cauldron pool (hash of its creation outpoint)"
);
