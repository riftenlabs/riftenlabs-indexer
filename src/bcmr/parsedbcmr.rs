// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

// The ParsedBCMR struct mirrors identity struct in the BCMR definition

use anyhow::bail;
use anyhow::{Context, Result};
use bitcoin_hashes::hex::FromHex;
use bitcoin_hashes::hex::ToHex;
use bitcoincash::TokenID;
use log::debug;
use serde::Serialize;
use serde_json::json;
use serde_json::Value;

const MAX_SYMBOL_LEN: usize = 100;
const MAX_NAME_LEN: usize = 500;
const MAX_DESCRIPTION_LEN: usize = 10000;
const MAX_URL_LEN: usize = 2000;

pub const SOURCE_ON_CHAIN: &str = "onchain";

#[derive(Serialize)]
pub struct Token {
    pub category: String,
    pub symbol: String,
    pub decimals: usize,
}

#[derive(Serialize)]
pub struct Uris {
    pub icon: Option<String>,
    pub web: Option<String>,
}

#[derive(Serialize)]
pub struct FileMeta {
    pub expected_hash: Option<String>,
    pub actual_hash: Option<String>,
    pub source: String,
}

#[derive(Serialize)]
pub struct ParsedBCMR {
    pub name: String,
    pub description: String,
    pub token: Token,
    pub uris: Uris,
    pub filemeta: FileMeta,
}

fn trim_str(str: &str, new_size: usize) -> String {
    let mut iter = str.char_indices();

    let (start, _) = match iter.next() {
        Some(i) => i,
        None => {
            // not sure why this should fail (empty string?)
            return str.to_owned();
        }
    };
    let (end, _) = match iter.nth(new_size) {
        Some(i) => i,
        None => {
            return str.to_owned();
        }
    };

    str[start..end].to_owned() + "..."
}

impl ParsedBCMR {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        category: &str,
        symbol: &str,
        decimals: usize,
        name: &str,
        description: &str,
        icon: &str,
        web: &str,
        expected_hash: Option<String>,
        actual_hash: Option<String>,
        source: &str,
    ) -> Self {
        // Trim inputs so avoid bogus BCMR files populating our db with garbage

        Self {
            name: trim_str(name, MAX_NAME_LEN),
            description: trim_str(description, MAX_DESCRIPTION_LEN),
            token: Token {
                category: category.to_owned(),
                symbol: trim_str(symbol, MAX_SYMBOL_LEN),
                decimals,
            },
            uris: Uris {
                icon: if icon.is_empty() {
                    None
                } else {
                    Some(trim_str(icon, MAX_URL_LEN))
                },
                web: if web.is_empty() {
                    None
                } else {
                    Some(trim_str(web, MAX_URL_LEN))
                },
            },
            filemeta: FileMeta {
                expected_hash,
                actual_hash,
                source: source.into(),
            },
        }
    }
}

fn parse_token_identity_history(
    token_history: &Value,
    token_id: &TokenID,
    expected_hash: Option<String>,
    actual_hash: Option<String>,
    source: &str,
) -> Result<ParsedBCMR> {
    let first_entry = token_history
        .as_object()
        .context("token history is not an object")?
        .values()
        .next()
        .context("no entries in token history")?;

    let token = first_entry
        .get("token")
        .context("'token' missing in token history entry")?;

    let category = token
        .get("category")
        .context("category missing from 'token'")?
        .as_str()
        .context("'category' field in 'token' not a string")?;
    if category.to_lowercase() != token_id.to_hex().to_lowercase() {
        bail!(
            "Mismatch on 'token'->'category' entry and expected category ({} != {}",
            category,
            token_id.to_hex()
        );
    }

    let symbol = token
        .get("symbol")
        .context("'symbol' missing in 'token' entry")?;
    let symbol = symbol.as_str().context("'symbol' field is not a string")?;

    // decimals is optional and defaults to 0
    let decimals_default = json!(0);
    let decimals = token
        .get("decimals")
        .unwrap_or(&decimals_default)
        .to_owned();

    let decimals = {
        let d_int = if let Some(d) = decimals.as_i64() {
            d
        } else if let Some(d_str) = decimals.as_str() {
            d_str
                .parse::<i64>()
                .context("Invalid number in 'decimal' field for 'token")?
        } else {
            bail!("BCMR contains a 'decimal' field for 'token', but it's not a number")
        };

        if d_int < 0 {
            bail!("'decimal' field for 'token' cannot be negative")
        }
        d_int as usize
    };

    let empty_string = json!("");
    let name = first_entry
        .get("name")
        .unwrap_or(&empty_string)
        .as_str()
        .unwrap_or("");
    let description = first_entry
        .get("description")
        .unwrap_or(&empty_string)
        .as_str()
        .unwrap_or("");

    let (icon, web) = if let Some(uris) = first_entry.get("uris") {
        let icon = uris
            .get("icon")
            .unwrap_or(&empty_string)
            .as_str()
            .unwrap_or("");
        let web = uris
            .get("web")
            .unwrap_or(&empty_string)
            .as_str()
            .unwrap_or("");
        (icon, web)
    } else {
        ("", "")
    };

    Ok(ParsedBCMR::new(
        &token_id.to_hex(),
        symbol,
        decimals,
        name,
        description,
        icon,
        web,
        expected_hash,
        actual_hash,
        source,
    ))
}

pub fn parse_all_bcmr_identities(
    bcmr: &Value,
    ignore_errors: bool,
    source: &str,
) -> Result<Vec<ParsedBCMR>> {
    let identities = bcmr
        .get("identities")
        .context("BCMR missing 'identities'")?;

    let identities = identities
        .as_object()
        .context("BCMR identities not an object")?;

    let mut tokens: Vec<ParsedBCMR> = Vec::default();
    for (token_id, history) in identities.into_iter() {
        let token_id =
            TokenID::from_hex(token_id).context("Invalid token ID in identities object")?;
        let bcmr = match parse_token_identity_history(history, &token_id, None, None, source) {
            Ok(bcmr) => bcmr,
            Err(e) => {
                if ignore_errors {
                    debug!(
                        "parse bcmr: ignoring token {} due to error {}",
                        token_id.to_hex(),
                        e
                    );
                    continue;
                } else {
                    return Err(e);
                }
            }
        };
        tokens.push(bcmr)
    }

    Ok(tokens)
}

pub fn parse_bcmr_json(
    bcmr: &Value,
    token_id: &TokenID,
    actual_hash: Option<String>,
    expected_hash: Option<String>,
    source: &str,
) -> Result<ParsedBCMR> {
    let identities = bcmr
        .get("identities")
        .context("BCMR missing 'identities'")?;
    let token_history = identities
        .get(token_id.to_hex())
        .context("'identities' does not contain token ID")?;

    parse_token_identity_history(token_history, token_id, expected_hash, actual_hash, source)
}

#[cfg(test)]
mod tests {

    use super::*;
    use bitcoin_hashes::{hex::FromHex, Hash};

    #[test]
    fn test_bcmr_parse() {
        let bcmr = r#"
        {
            "$schema": "https://cashtokens.org/bcmr-v2.schema.json",
            "version": {
              "major": 0,
              "minor": 1,
              "patch": 0
            },
            "latestRevision": "2023-05-15T12:18:32.912Z",
            "registryIdentity": {
              "name": "Fallout Coin",
              "description": "One of the earliest coins ever created on BCH",
              "uris": {
                "icon": "https://c3-soft.com/tokens/icon.ico",
                "web": "https://c3-soft.com/tokens/",
                "registry": "https://c3-soft.com/tokens/registry.json"
              }
            },
            "identities": {
              "83e12eea20b19a9a0906bb0521ff18520db69a4a8136293bafbfca0acb2c2313": {
                "2023-05-12T12:00:00.000Z": {
                  "name": "Fallout Coin",
                  "description": "Commemorative Coin for the computer game Fallout",
                  "token": {
                    "category": "83e12eea20b19a9a0906bb0521ff18520db69a4a8136293bafbfca0acb2c2313",
                    "symbol": "FC",
                    "decimals": 2
                  },
                  "uris": {
                    "icon": "https://c3-soft.com/tokens/fallout.ico"
                  }
                }
              }
            },
            "license": "CC0-1.0"
          }
          "#;

        let dummy_hash = Some(TokenID::all_zeros().to_hex());

        let parsed = parse_bcmr_json(
            &serde_json::from_str(bcmr).unwrap(),
            &TokenID::from_hex("83e12eea20b19a9a0906bb0521ff18520db69a4a8136293bafbfca0acb2c2313")
                .unwrap(),
            dummy_hash.clone(),
            dummy_hash,
            &SOURCE_ON_CHAIN,
        )
        .unwrap();

        assert_eq!("FC", parsed.token.symbol);
        assert_eq!(2, parsed.token.decimals);
        assert_eq!("Fallout Coin", parsed.name);
        assert_eq!(
            "Commemorative Coin for the computer game Fallout",
            parsed.description
        );
        assert_eq!(
            "https://c3-soft.com/tokens/fallout.ico",
            parsed.uris.icon.unwrap()
        );
        assert_eq!(None, parsed.uris.web);
    }

    #[test]
    fn test_parse_all() {
        let bcmr = r#"
        {
            "$schema": "https://cashtokens.org/bcmr-v2.schema.json",
            "version": { "major": 1, "minor": 0, "patch": 0 },
            "latestRevision": "2023-05-13T00:00:00.000Z",
            "registryIdentity": {
                "name": "OpenTokenRegistry",
                "description": "A community-reviewed registry of tokens on Bitcoin Cash. Publishes names, symbols, icons, and other metadata about CashTokens via a Bitcoin Cash Metadata Registry (BCMR).",
                "uris": {
                "icon": "https://otr.cash/img/open-token-registry-icon.png",
                "web": "https://otr.cash/",
                "registry": "https://otr.cash/.well-known/bitcoin-cash-metadata-registry.json"
                }
            },
            "identities": {
                "07a70ec6e0a325991e829daea5de1be1bb71e1ef4a04931cdddf567d8f60f676": {
                    "2023-05-12T20:18:04.000Z": {
                        "name": "OTR Chipnet Tokens",
                        "description": "OpenTokenRegistry's example fungible token issued on Chipnet, a network used for Bitcoin Cash development.\n\nIn user interfaces with limited space, descriptions may be hidden beyond 140 characters or the first newline character; this example allows for testing of hiding and revealing extended descriptions.",
                        "token": {
                        "category": "0afd5f9ad130d043f627fad3b422ab17cfb5ff0fc69e4782eea7bd0853948428",
                        "decimals": 6,
                        "symbol": "OTRC"
                        },
                        "splitId": "00000000040ba9641ba98a37b2e5ceead38e4e2930ac8f145c8094f94c708727",
                        "uris": {
                        "icon": "ipfs://bafkreig32k6kowcldesjbytktvsh47wnr72qp67ig3aldcjkkkgkx4jmnq",
                        "web": "https://otr.cash/docs/test",
                        "chat": "https://t.me/cashtoken_devs"
                        }
                    }
                },

            "180f0db4465c2af5ef9363f46bacde732fa6ffb3bfe65844452078085b2e7c93": {
                "2023-05-26T17:10:00.000Z": {
                    "name": "Emerald DAO U23",
                    "description": "A series of 2000 keycard NFTs which entitle each holder to a unique, on-chain Emerald DAO safebox holding at least 0.101 BCH. Beginning May 15, 2024, each keycard NFT may be burned to unlock its matching safebox.\n\nThe Emerald DAO U23 series was created on May 15, 2023 to demonstrate new capabilities enabled by the CashTokens upgrade and to celebrate the 2023 upgrade event. Emerald DAO U23 is minted by an instance of Emerald DAO v2.1.0 with a minimum safebox size of 0.1 BCH and an initial endowment of 2 BCH into DAO's rewards pool.",
                    "token": {
                    "category": "180f0db4465c2af5ef9363f46bacde732fa6ffb3bfe65844452078085b2e7c93",
                    "symbol": "EMRDAO-U23"
                    },
                    "uris": {
                    "icon": "ipfs://bafybeigqogqx3n4cldk6nizl5vihopi2dkgvw4yqamc5rhleyqu25soe4e",
                    "web": "https://emerald-dao.cash/",
                    "chat": "https://t.me/emeralddao",
                    "app": "https://emerald-dao.vercel.app"
                    }
                }
            },

            "482d555258d3be69fef6ffcd0e5eeb23c4aaacec572b25ab1c21897600c45887": {
                "2023-07-19T16:23:41.467Z": {
                    "name": "Real Bitcoin Fam Community Award Tokens",
                    "description": "Real Bitcoin Fam's community award token for rewarding new BCH builders who are building with CashTokens. Real Bitcoin Fam is a support network for Bitcoin, cryptocurrency and Web3 builders who align with an OG Bitcoin vibe.",
                    "token": {
                        "category": "482d555258d3be69fef6ffcd0e5eeb23c4aaacec572b25ab1c21897600c45887",
                        "symbol": "XRBF",
                        "decimals": 2
                    },
                    "uris": {
                        "icon": " ipfs://bafkreiax4h2evyf4g7iuu6kuqkxwez4kvduxayngj5vxmvgrekipevptl4",
                        "web": "https://RealBitcoinFam.com",
                        "youtube": "https://www.youtube.com/@RealBitcoinCashSite",
                        "support": "https://t.me/Panmoni",
                        "telegram": "https://t.me/Panmoni",
                        "twitter": "https://twitter.com/RealBitcoinFam",
                        "discord": "https://discord.gg/MaybgkHs53",
                        "instagram": "https://www.instagram.com/realbitcoinfam/"
                    },
                    "extensions": {
                        "contact": {
                            "phone": "+1 (215) 360-3513",
                            "email": "hello@panmoni.com"
                        }
                    }
                }
            }
        },
        "license": "CC0-1.0"
        }"#;
        let parsed =
            parse_all_bcmr_identities(&serde_json::from_str(bcmr).unwrap(), true, "otr").unwrap();

        // test BCMR has 3 entries, but one has a category mismatch and is ignored
        assert_eq!(2, parsed.len());
        assert!(parsed.iter().any(|bcmr| bcmr.token.category
            == "180f0db4465c2af5ef9363f46bacde732fa6ffb3bfe65844452078085b2e7c93"));
        assert!(parsed.iter().any(|bcmr| bcmr.token.category
            == "482d555258d3be69fef6ffcd0e5eeb23c4aaacec572b25ab1c21897600c45887"));

        // should fail (as we're not ignoring errors)
        let parsed = parse_all_bcmr_identities(&serde_json::from_str(bcmr).unwrap(), false, "otr");
        assert!(parsed.is_err());
    }
}
