// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use rocket::get;
use rocket::{http::Status, response::status::Custom, serde::json::Json, State};
use serde_json::json;
use serde_json::Value;

use crate::db::{cauldron::user::get_unique_per_month_accumilating, DB};

#[get("/user/unique_addresses")]
pub fn unique_addresses(conn: &State<DB>) -> Result<Json<Value>, Custom<String>> {
    let db = conn.cauldron_r.get().map_err(|_| {
        Custom(
            Status::InternalServerError,
            "Failed to get DB connection".into(),
        )
    })?;

    let users = get_unique_per_month_accumilating(&db)
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;

    Ok(Json(json!(users)))
}
