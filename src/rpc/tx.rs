// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use bitcoin_hashes::hex::FromHex;
use bitcoincash::TokenID;
use rocket::{get, http::Status, response::status::Custom, serde::json::Json, State};
use serde_json::json;
use serde_json::Value;

use crate::db::DB;

#[get("/tx/latest?<limit>&<offset>&<token>")]
pub fn tx_latest(
    limit: Option<usize>,
    offset: Option<usize>,
    token: Option<&str>,
    conn: &State<DB>,
) -> Result<Json<Value>, Custom<String>> {
    let db = conn.cauldron_r.get().map_err(|_| {
        Custom(
            Status::InternalServerError,
            "Failed to get DB connection".into(),
        )
    })?;

    let limit = limit.unwrap_or(100).min(10000);
    let offset = offset.unwrap_or(0);

    let token = match token {
        None => None,
        Some(tokenhex) => match TokenID::from_hex(tokenhex) {
            Ok(token) => Some(token),
            Err(_) => return Err(Custom(Status::BadRequest, "Invalid token ID".into())),
        },
    };

    let txs = crate::db::cauldron::tx::latest(&db, limit, offset, token)
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;

    let txs_json: Vec<Value> = txs
        .into_iter()
        .map(|tx| {
            json!({
                "txid": tx.0.to_string(),
                "blockhash": tx.1.map(|blockhex| blockhex.to_string()),
                "timestamp_guess": tx.2
            })
        })
        .collect();

    Ok(Json(json!(txs_json)))
}
