// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use rocket::{http::Status, response::status::Custom};

pub fn to_internal_error<E: std::fmt::Display>(e: E) -> Custom<String> {
    Custom(Status::InternalServerError, format!("Error: {}", e))
}
