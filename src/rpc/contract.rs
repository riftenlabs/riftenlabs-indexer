// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use rayon::prelude::*;
use rocket::{get, http::Status, response::status::Custom, serde::json::Json, State};
use rusqlite::Connection;
use serde::Serialize;
use serde_json::{json, Value};

use crate::{db::DB, timeutil::time_now};

#[derive(Serialize)]
pub struct ContractCount {
    active: u64,
    ended: u64,
    interactions: u64,
}

fn db_contract_count_all(db: &Connection) -> Result<ContractCount> {
    let active: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NULL",
        [],
        |row| row.get(0),
    )?;

    let ended: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NOT NULL",
        [],
        |row| row.get(0),
    )?;

    let interactions: u64 = db.query_row("SELECT COUNT(*) FROM pool_history_entry", [], |row| {
        row.get(0)
    })?;

    Ok(ContractCount {
        active,
        ended,
        interactions,
    })
}

fn db_contract_count_by_token(db: &Connection, token_id: &str) -> Result<ContractCount> {
    let active: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NULL AND token_id = ?",
        [token_id],
        |row| row.get(0),
    )?;

    let ended: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NOT NULL AND token_id = ?",
        [token_id],
        |row| row.get(0),
    )?;

    let interactions: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool_history_entry phe JOIN pool p ON phe.pool = p.creation_utxo WHERE p.token_id = ?",
        [token_id],
        |row| row.get(0),
    )?;

    Ok(ContractCount {
        active,
        ended,
        interactions,
    })
}

#[get("/contract/count")]
pub fn contract_count_all(conn: &State<DB>) -> Result<Json<Value>, Custom<String>> {
    let db = conn
        .cauldron_r
        .get()
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;
    let count = db_contract_count_all(&db)
        .map_err(|e| Custom(Status::BadRequest, format!("Error: {}", e)))?;
    Ok(Json(json!(count)))
}

#[get("/contract/count/<token>")]
pub fn contract_count_token(token: &str, conn: &State<DB>) -> Result<Json<Value>, Custom<String>> {
    let db = conn
        .cauldron_r
        .get()
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;
    let count = db_contract_count_by_token(&db, token)
        .map_err(|e| Custom(Status::BadRequest, format!("Error: {}", e)))?;
    Ok(Json(json!(count)))
}

#[get("/contract/volume?<end>")]
pub fn contract_volume(
    end: Option<i64>,
    conn: &State<DB>,
) -> Result<Json<Vec<Value>>, Custom<String>> {
    let end_timestamp = match end {
        Some(e) => e,
        None => time_now(),
    };

    let db = conn
        .cauldron_r
        .get()
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;
    let volume = super::contract_volume(&db, end_timestamp as u64)
        .map_err(|e| Custom(Status::BadRequest, format!("Error: {}", e)))?;

    let result: Vec<Value> = volume
        .into_par_iter()
        .map(|(token, (total, thirty_days, one_day))| {
            json!({
                "token_id": token,
                "total_sats": total,
                "thirty_days_sats": thirty_days,
                "one_day_sats": one_day
            })
        })
        .collect();

    Ok(Json(result))
}
