// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use bitcoincash::{TokenID, Transaction};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use rusqlite::Connection;

pub mod crc20fetcher;

use crate::{db::crc20::insert_crc20_candidate, utiltoken::is_genesis_tx};

pub fn index_crc20(conn: &Connection, txs: &Vec<Transaction>) -> Result<()> {
    let token_genesis: Vec<TokenID> = txs.par_iter().filter_map(is_genesis_tx).collect();

    for token_id in token_genesis {
        insert_crc20_candidate(conn, &token_id)?
    }

    Ok(())
}
