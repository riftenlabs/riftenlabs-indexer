// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use std::{
    sync::{atomic::AtomicBool, Arc, Mutex},
    thread::{self, JoinHandle},
    time::Duration,
};

use crate::db::crc20::{
    bump_failed_attempts, get_not_indexed_tokens, update_to_crc20, update_to_not_crc20,
};
use crate::db::DBPool;
use anyhow::*;
use electrum_client_netagnostic::{Client, ElectrumApi, Param};
use log::{info, warn};
use rand::thread_rng;
use serde_json::Value;

use rand::seq::SliceRandom;
use std::result::Result::Ok;

pub struct CRC20Fetcher {
    keep_running: Arc<AtomicBool>,
    update_thread: Option<JoinHandle<()>>,
}

const LOOP_SLEEP_TIME: Duration = Duration::from_secs(10);

fn parse_crc20_info(json: &Value) -> Option<(String, String, i32)> {
    let crc20 = json.get("crc20");
    if crc20.is_none() || crc20 == Some(&Value::Null) {
        return None;
    }

    let symbol = crc20
        .and_then(|c| c.get("symbol"))
        .and_then(|s| s.as_str())
        .map(|s| s.to_string());

    let name = crc20
        .and_then(|c| c.get("name"))
        .and_then(|n| n.as_str())
        .map(|n| n.to_string());

    let decimals = crc20
        .and_then(|c| c.get("decimals"))
        .and_then(|d| d.as_i64())
        .map(|d| d as i32);

    Some((
        symbol.unwrap_or_default(),
        name.unwrap_or_default(),
        decimals.unwrap_or_default(),
    ))
}

impl CRC20Fetcher {
    pub fn new() -> Self {
        Self {
            keep_running: Arc::new(AtomicBool::new(true)),
            update_thread: None,
        }
    }

    pub fn start(&mut self, db: DBPool, electrum: Arc<Mutex<Client>>) -> Result<()> {
        let keep_running_cpy = self.keep_running.clone();
        self.update_thread = Some(
            thread::Builder::new()
                .name("crc20_fetcher".to_string())
                .spawn(move || loop {
                    if !keep_running_cpy.load(std::sync::atomic::Ordering::Relaxed) {
                        info!("Exiting crc20 fetcher thread");
                        return;
                    }

                    let mut queue = {
                        let db = match db.get() {
                            Ok(db) => db,
                            Err(e) => {
                                warn!("Failed to get crc20 db connection {}", e);
                                thread::sleep(LOOP_SLEEP_TIME);
                                continue;
                            }
                        };
                        match get_not_indexed_tokens(&db) {
                            Ok(tokens) => tokens,
                            Err(e) => {
                                warn!("Failed to get unindexed crc20 tokens {}", e);
                                thread::sleep(LOOP_SLEEP_TIME);
                                continue;
                            }
                        }
                    };

                    // in case electrum has issues with a token; with rng we'll eventually get all others
                    let mut rng = thread_rng();
                    queue.shuffle(&mut rng);

                    if queue.is_empty() {
                        thread::sleep(LOOP_SLEEP_TIME);
                        continue;
                    }

                    info!("crc20: {} tokens need fetching", queue.len());

                    for token in queue {
                        let genesis_info = match electrum
                            .lock()
                            .unwrap()
                            .raw_call("token.genesis.info", vec![Param::String(token.clone())])
                        {
                            Ok(i) => Some(i),
                            Err(e) => {
                                info!("Failed to fetch crc20 info for {}: {}", token, e);
                                None
                            }
                        };

                        let db = match db.get() {
                            Ok(db) => db,
                            Err(e) => {
                                warn!("Failed to get crc20 db connection {}", e);
                                break;
                            }
                        };

                        if genesis_info.is_none() {
                            if let Err(e) = bump_failed_attempts(&db, &token) {
                                warn!("Failed to bump failed attempts for {}: {}", token, e);
                            }
                            continue;
                        }

                        let res = if let Some((symbol, name, decimals)) =
                            parse_crc20_info(&genesis_info.unwrap())
                        {
                            update_to_crc20(&db, &token, &symbol, &name, decimals)
                        } else {
                            update_to_not_crc20(&db, &token)
                        };

                        if let Err(e) = res {
                            warn!("Failed to update crc20 for token {}: {}", token, e);
                        }
                    }

                    thread::sleep(LOOP_SLEEP_TIME)
                })
                .expect("failed to start bcmr download thread"),
        );

        Ok(())
    }
}

impl Drop for CRC20Fetcher {
    fn drop(&mut self) {
        self.keep_running
            .store(false, std::sync::atomic::Ordering::SeqCst);
        if let Some(thread) = self.update_thread.take() {
            // Wake the thread in case it is sleeping
            thread.thread().unpark();

            match thread.join() {
                Ok(_) => info!("crc20 fetcher thread done"),
                Err(e) => warn!("Failed to join crc20 fetcher thread: {:?}", e),
            }
        }
    }
}
