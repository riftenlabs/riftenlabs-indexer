// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

#[cfg(test)]
mod test_utils {
    use crate::db::DB;
    use r2d2::Pool;
    use r2d2_sqlite::SqliteConnectionManager;
    use rusqlite::Connection;
    use std::sync::Arc;

    pub fn mock_db_pool<F>(setup_fn: F) -> DB
    where
        F: Fn(&Connection),
    {
        let manager = SqliteConnectionManager::memory();
        let pool = Pool::new(manager).expect("Failed to create pool.");

        let write_conn = pool.get().expect("Failed to get connection.");
        setup_fn(&write_conn);

        DB {
            cauldron_w: Arc::new(pool.clone()),
            cauldron_r: Arc::new(pool.clone()),
            bcmr_w: Arc::new(pool.clone()),
            bcmr_r: Arc::new(pool.clone()),
            crc20_w: Arc::new(pool.clone()),
            crc20_r: Arc::new(pool.clone()),
        }
    }
}

#[cfg(test)]
pub use test_utils::mock_db_pool;
