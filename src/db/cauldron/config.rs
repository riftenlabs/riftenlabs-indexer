// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use rusqlite::{params, Connection};

/// Add or update config entry
pub fn config_set(conn: &Connection, key: &str, value: &str) {
    let mut stmt = conn
        .prepare("INSERT OR REPLACE INTO config (key, value) VALUES (?, ?)")
        .unwrap();
    stmt.execute(params![key, value]).unwrap();
}

/// Get config entry
pub fn config_get(conn: &Connection, key: &str) -> Result<Option<String>> {
    let mut stmt = conn.prepare("SELECT value FROM config WHERE key = ?")?;

    let mut row = stmt.query([key])?;
    if let Some(r) = row.next()? {
        Ok(r.get(0)?)
    } else {
        Ok(None)
    }
}
