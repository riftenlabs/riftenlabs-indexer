// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use rusqlite::Connection;

use crate::{
    bcmr::parsedbcmr::ParsedBCMR,
    db::bcmr::{get_token_bcmr, get_well_known_bcmr},
};

use super::pool::db_list_active_pools;

#[derive(Serialize)]
pub struct TokenListItem {
    pub token_id: String,
    pub trade_volume: u64,
    pub tvl_sats: u64,
    pub tvl_tokens: u64,
    pub bcmr_well_known: Vec<ParsedBCMR>,
    pub bcmr: Option<ParsedBCMR>, // on-chain

    // deprecated: set to 0 for backward compatibility
    pub trade_count: u64,
}

pub fn db_list_tokens_by_volume(
    cauldron_conn: &Connection,
    bcmr_conn: &Connection,
    seconds: usize,
    limit: usize,
) -> Result<Vec<TokenListItem>> {
    // List token ID's by volume last n seconds
    let mut statement = cauldron_conn
        .prepare(
            "
            WITH TradeData AS (
                SELECT
                    uf1.token_id,
                    ABS(uf1.sats - COALESCE(uf2.sats, 0)) as trade_volume
                FROM utxo_funding uf1
                INNER JOIN utxo_funding uf2 ON uf1.spent_utxo_hash = uf2.new_utxo_hash
                JOIN tx ON uf1.txid = tx.txid
                WHERE COALESCE(tx.first_seen_timestamp, tx.mtp_timestamp) >= (strftime('%s', 'now') - ?1)
            ),
            AllTokenIDs AS (
                SELECT DISTINCT token_id
                FROM pool
            ),
            AggregateTradeData AS (
                SELECT
                    all_tokens.token_id AS token_id,
                    COALESCE(SUM(td.trade_volume), 0) AS total_trade_volume
                FROM AllTokenIDs all_tokens
                LEFT JOIN TradeData td ON all_tokens.token_id = td.token_id
                GROUP BY all_tokens.token_id
            )
            SELECT
                token_id,
                total_trade_volume
            FROM AggregateTradeData
            ORDER BY total_trade_volume DESC
            LIMIT ?2
        ",
        )?;

    let mut query = statement.query([seconds, limit])?;

    let mut tokens: Vec<TokenListItem> = Vec::with_capacity(limit);

    while let Some(row) = query.next()? {
        let token_id: String = row.get(0)?;
        let trade_volume: u64 = row.get(1)?;

        let (tvl_sats, tvl_tokens) =
            db_list_active_pools(Some(&token_id), None, cauldron_conn, false)?
                .into_iter()
                .fold((0, 0), |tvl, pool| (tvl.0 + pool.sats, tvl.1 + pool.tokens));

        if tvl_sats == 0 {
            continue;
        }

        let bcmr = get_token_bcmr(bcmr_conn, &token_id)?;
        let bcmr_well_known = get_well_known_bcmr(bcmr_conn, &token_id)?;

        tokens.push(TokenListItem {
            token_id,
            trade_volume,
            tvl_sats,
            tvl_tokens,
            bcmr,
            bcmr_well_known,
            trade_count: 0,
        })
    }

    Ok(tokens)
}
