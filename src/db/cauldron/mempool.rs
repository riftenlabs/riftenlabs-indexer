// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use std::collections::HashSet;

use anyhow::Result;
use bitcoin_hashes::hex::{FromHex, ToHex};
use bitcoincash::Txid;
use rusqlite::Connection;

pub fn load_mempool(conn: &Connection) -> Result<HashSet<Txid>> {
    let mut stmt = conn.prepare("SELECT txid FROM tx WHERE blockhash is NULL")?;
    let txid_iter = stmt.query_map([], |row| row.get(0))?;

    let mut txids: HashSet<Txid> = HashSet::new();
    for txid_res in txid_iter {
        let txid_hex: String = txid_res?;
        let txid = Txid::from_hex(&txid_hex).expect("invalid txid in db");
        txids.insert(txid);
    }
    Ok(txids)
}

pub fn delete_mempool_txs<'a, I>(db_tx: &Connection, txids: I) -> Result<bool>
where
    I: IntoIterator<Item = &'a Txid>,
{
    let txid_hexes: Vec<String> = txids.into_iter().map(|txid| txid.to_hex()).collect();
    let placeholders = txid_hexes
        .iter()
        .map(|_| "?")
        .collect::<Vec<_>>()
        .join(", ");
    let query = format!(
        "DELETE FROM tx WHERE txid IN ({}) AND blockhash is NULL",
        placeholders
    );

    let params: Vec<&dyn rusqlite::ToSql> = txid_hexes
        .iter()
        .map(|s| s as &dyn rusqlite::ToSql)
        .collect();

    let mut stmt = db_tx.prepare(&query)?;
    let rows_deleted = stmt.execute(rusqlite::params_from_iter(params))?;

    Ok(rows_deleted != 0)
}
