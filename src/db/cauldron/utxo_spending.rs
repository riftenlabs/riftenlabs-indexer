// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoincash::Txid;
use riftenlabs_defi::cauldron::ParsedContract;
use rusqlite::{params, Connection, Transaction};

pub fn create_table(conn: &Connection) {
    conn.execute(
        "CREATE TABLE utxo_spending (
        spent_utxo_hash TEXT PRIMARY KEY,
        txid TEXT,
        FOREIGN KEY(txid) REFERENCES tx(txid) ON DELETE CASCADE
    )",
        [],
    )
    .unwrap();
}

pub fn insert_utxo_spending(
    tx: &Transaction,
    cauldrons: &Vec<ParsedContract>,
    txid: &Txid,
    replace: bool,
) -> Result<()> {
    let mut statement = tx.prepare(&format!(
        "INSERT OR {} INTO utxo_spending (spent_utxo_hash, txid) VALUES (?, ?)",
        if replace { "REPLACE" } else { "IGNORE " }
    ))?;

    for c in cauldrons {
        statement.execute(params![c.spent_utxo_hash.to_hex(), txid.to_hex(),])?;
    }
    Ok(())
}
