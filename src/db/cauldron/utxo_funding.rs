// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoincash::Txid;
use riftenlabs_defi::cauldron::ParsedContract;
use rusqlite::{params, Connection};

pub fn create_table(conn: &Connection) {
    conn.execute(
        "CREATE TABLE utxo_funding (
            new_utxo_hash TEXT PRIMARY KEY,
            txid TEXT,
            spent_utxo_hash TEXT,
            new_utxo_txid TEXT,
            new_utxo_n INT,
            sats BIGINT,
            token_amount BIGINT,
            token_id TEXT,
            FOREIGN KEY(txid) REFERENCES tx(txid) ON DELETE CASCADE
        )",
        [],
    )
    .unwrap();
}

pub fn insert_utxo_funding(
    con: &Connection,
    cauldrons: &Vec<ParsedContract>,
    txid: &Txid,
    replace: bool,
) -> Result<()> {
    let mut statement = con
        .prepare(
            &format!("INSERT OR {} INTO utxo_funding (new_utxo_hash, txid, spent_utxo_hash, new_utxo_txid, new_utxo_n, sats, token_amount, token_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            if replace {
                "REPLACE"
            } else { "IGNORE"}
        ))?;

    for c in cauldrons {
        if c.new_utxo_hash.is_none() {
            continue;
        }
        statement.execute(params![
            c.new_utxo_hash.unwrap().to_hex(),
            txid.to_hex(),
            c.spent_utxo_hash.to_hex(),
            c.new_utxo_txid.unwrap().to_hex(),
            c.new_utxo_n.unwrap(),
            c.sats.unwrap(),
            c.token_amount.unwrap(),
            c.token_id.unwrap().to_hex(),
        ])?;
    }
    Ok(())
}
