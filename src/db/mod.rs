// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use std::sync::Arc;

pub mod bcmr;
pub mod cauldron;
pub mod crc20;
pub mod search;

pub type DBPool = Arc<r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>>;

#[derive(Clone)]
pub struct DB {
    // readwrite
    pub cauldron_w: DBPool,
    // readonly
    pub cauldron_r: DBPool,
    // readonly
    pub bcmr_r: DBPool,
    // readwrite
    pub bcmr_w: DBPool,
    // rewadwrite
    pub crc20_w: DBPool,
    // readonly
    #[allow(dead_code)]
    pub crc20_r: DBPool,
}
