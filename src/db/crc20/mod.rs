// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoincash::TokenID;
use rusqlite::{params, Connection};

const STATE_NOT_INDEXED: i32 = -1;
const STATE_NOT_CRC20: i32 = 0;
const STATE_IS_CRC20: i32 = 1;

const MAX_FAILED_ATTEMPTS: i32 = 20;

pub fn prepare_tables(conn: &Connection) {
    conn.execute(
        "CREATE TABLE crc20 (
            token_id TEXT PRIMARY KEY,
            symbol TEXT NOT NULL,
            name TEXT NOT NULL,
            decimals INT NOT NULL
        )",
        [],
    )
    .expect("failed to create crc20 table");

    conn.execute(
        "CREATE TABLE crc20_candidates (
            token_id TEXT PRIMARY KEY,
            is_crc20 INT NOT NULL,
            failed_attempts INT NOT NULL
        )",
        [],
    )
    .expect("failed to create crc20 table");
}

pub fn insert_crc20_candidate(conn: &Connection, token_id: &TokenID) -> Result<()> {
    conn.execute(
        "INSERT OR IGNORE INTO crc20_candidates (token_id, is_crc20, failed_attempts)
         VALUES (?1, ?2, ?3)",
        params![token_id.to_hex(), STATE_NOT_INDEXED, 0],
    )
    .map_err(|e| {
        anyhow::anyhow!(
            "failed to insert crc20 candidate: token_id = {}. Original error: {:?}",
            token_id.to_hex(),
            e
        )
    })?;

    Ok(())
}

pub fn get_not_indexed_tokens(conn: &Connection) -> Result<Vec<String>> {
    let mut stmt = conn.prepare(
        "SELECT token_id FROM crc20_candidates WHERE is_crc20 = ?1 AND failed_attempts <= ?2",
    )?;
    let token_ids = stmt
        .query_map(params![STATE_NOT_INDEXED, MAX_FAILED_ATTEMPTS], |row| {
            row.get(0)
        })?
        .collect::<Result<Vec<String>, _>>()?;

    Ok(token_ids)
}

pub fn update_to_not_crc20(conn: &Connection, token: &str) -> Result<()> {
    conn.execute(
        "UPDATE crc20_candidates SET is_crc20 = ?1 WHERE token_id = ?2",
        params![STATE_NOT_CRC20, token],
    )
    .map_err(|e| {
        anyhow::anyhow!(
            "failed to update token_id = {} to STATE_NOT_CRC20. Original error: {:?}",
            token,
            e
        )
    })?;

    Ok(())
}

pub fn update_to_crc20(
    conn: &Connection,
    token_id: &str,
    symbol: &str,
    name: &str,
    decimals: i32,
) -> Result<()> {
    // Update is_crc20 in crc20_candidates table
    conn.execute(
        "UPDATE crc20_candidates SET is_crc20 = ?1 WHERE token_id = ?2",
        params![STATE_IS_CRC20, token_id],
    )
    .map_err(|e| {
        anyhow::anyhow!(
            "failed to update token_id = {} to STATE_IS_CRC20 in crc20_candidates. Original error: {:?}",
            token_id,
            e
        )
    })?;

    // Insert or replace the token in the crc20 table
    conn.execute(
        "INSERT OR REPLACE INTO crc20 (token_id, symbol, name, decimals)
         VALUES (?1, ?2, ?3, ?4)",
        params![token_id, symbol, name, decimals],
    )
    .map_err(|e| {
        anyhow::anyhow!(
            "failed to insert or replace token_id = {} in crc20. Original error: {:?}",
            token_id,
            e
        )
    })?;

    Ok(())
}

pub fn bump_failed_attempts(conn: &Connection, token_id: &str) -> Result<()> {
    conn.execute(
        "UPDATE crc20_candidates SET failed_attempts = failed_attempts + 1 WHERE token_id = ?1",
        params![token_id],
    )
    .map_err(|e| {
        anyhow::anyhow!(
            "Failed to bump failed_attempts for token_id = {}. Original error: {:?}",
            token_id,
            e
        )
    })?;

    Ok(())
}
